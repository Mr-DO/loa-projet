#include <iostream>
#include <string>
#include <vector>
#include <bits/stdc++.h> 
#include <errno.h>
#include <map>
#include <cctype>
#include <iterator>
#include <random>
#include <signal.h>
#include <unistd.h>
#include <cstdlib>
#include <fstream>


// Forward declarations
class Position;
class Character;
class PositionIsAffected;

/**
 * Delimited Plateau definition
*/
class Plateau
{
private:
    // Perimetre plateau and compositions elements.
    int width;
    int height;
    int nbrElements;
    int sideElements;
    int insideElements;
    int reumus;
    int reumusInside;
    int teupors;
    int streumons;
    int diams;
    int geurchars;
    int intrus;

    // informations on the plateau
    int nbrTeleports = 0;
    int nbrGates = 0;
    int level = 1;

    // Variables for the collection all elements of the plateau and positions.
    std::vector<int> has_moving;
    std::map<int, Character*> side_affectations;
    std::map<int, Character*> inside_affectations;
    Character *player;

    // Initialisation for compositions elements.
    void initCharactersNumber();
    void initReumusSize();
    void initReumusInsideSize();
    void initTeuporsSize();
    void initStreumonsSize();
    void initDiamsSize();
    void initGeurcharsSize();

    // set variavle with collect all position and set default affected false
    void initializeCollectionPositions();

    // add element in side
    void addElementInside(char discriminant, char elem, int size);

    // add Teupors and Reumus side
    void addTeuporsReumusSide();

    // add reumus in side
    void addReumusInside();

    // add streumons in side
    void addStreumonsInside();

    // add diams in side
    void addDiamsInside();

    // add geurchars in side
    void addGeurcharsInside();

    // add intrus in side
    void addIntrusInside();

    // Peek plateau element
    int peekElementOfPlateau(std::map<std::string, std::string> plateau_elements, std::string key); 

    // Pre init the members variables before setting
    void preInit(int width, int height);

public:
    // Constructors of the plateau.
    Plateau();
    Plateau(int dim);
    Plateau(int width, int height);
    Plateau(std::string file_board);

    // put characters on the plateau
    void init();

    // Parsing file .board
    std::map<std::string, std::string> parse_file_board(std::string file_board);

    // getters
    const std::map<int, Character*> &get_side_affectations();
    const std::map<int, Character*> &get_inside_affectations();
    int getWidth();
    int getHeight();
    int getIndex(int i, int j);
    int getIndex(Position *pos);
    int getNbrTeleports();
    int getNbrGates();
    int getLevel();
    Character *getPlayer();

    // setters
    void setNbrTeleports(int teleports);
    void setNbrGates(int gates);
    void setLevel(int level);
    void setTeuporsSize(int teupors);
    void setReumusInsideSize(int reumusInside);
    void setStreumonsSize(int streumons);
    void setDiamsSize(int diams);
    void setGeurcharsSize(int geurchars);

    // increment functions
    void incNbrTeleport();
    void incNbrOpenGate();
    void incLevel();

    // decrement functions
    void decNbrTeleport();
    void decNbrOpenGate();

    // Open Random gate side
    void openAnGate();

    // move elements movable
    void move();

    // swap two Characters
    void swap(int key_1, int key_2);

    // marque element has moving
    void character_has_moving(int index);

    // display plateau.
    void displayHead();
    void displayFooter();
    void display();
    
    ~Plateau();
};

/**
 * Delimited Character definition
*/
class Character
{
protected:
    char form;
    Position *position;
    Plateau *plateau;
    std::vector<Position> itinerary;
    Character();
public:
    Character(Plateau *plateau, char form, Position *position);

    //getters
    char getForm();
    Position* getPosition();

    //setters
    void setForm(char form);
    void setPosition(Position *position);

    // atitude and movement of character
    virtual void attitude(Character *character) = 0;
    virtual bool move();
    virtual bool moveUp();
    virtual bool moveDown();
    virtual bool moveLeft();
    virtual bool moveRight();

    // Swap two Characters
    void swap(Character *character);

    // Display position
    void printPosition(); 

    virtual ~Character();
};

class CharacterStationary : public Character
{
private:
public:
    CharacterStationary(Plateau *plateau, char form, Position *position);

    // atitude and movement of character
    virtual void attitude(Character *character);

    ~CharacterStationary();
};

class CharacterMonster : public Character
{
private:
    bool move_(int i, int j);
public:
    CharacterMonster(Plateau *plateau, char form, Position *position);
    
    // atitude and movement of character
    virtual void attitude(Character *character);
    virtual bool move();
    virtual bool moveUp();
    virtual bool moveDown();
    virtual bool moveLeft();
    virtual bool moveRight();

    virtual ~CharacterMonster();
};

class CharacterPlayer : public Character
{
private:
    bool move_(int i, int j);
public:
    CharacterPlayer(Plateau *plateau, char form, Position *position);

    // atitude and movement of character
    virtual void attitude(Character *character);
    virtual bool move();
    virtual bool moveUp();
    virtual bool moveDown();
    virtual bool moveLeft();
    virtual bool moveRight();

    virtual ~CharacterPlayer();
};

// form of characters
class Form
{
private:
    Form();
public:
    static const char EMPTY = ' ';
    static const char REUMUS = 'X';
    static const char TEUPORS_CLOSE = '-';
    static const char TEUPORS_OPEN = '+';
    static const char STREUMONS = 'S';
    static const char DIAMS = '$';
    static const char GEURCHARS = '*';
    static const char INTRUS = 'J' ;
    static const char DEAD = 'D';
};

/**
 * Delimited Position definition
*/
class Position
{
private:
    int i;
    int j;
public:
    Position();
    Position(int i, int j);

    // getters
    int getI() const;
    int getJ() const;
    int getIndex(int width) const;

    // setters
    void setI(int i);
    void setJ(int j);
    ~Position();

    // Surchage operators
    bool operator<(const Position &pos) const;
    bool operator>=(const Position &pos) const;
    bool operator==(const Position &pos) const;
};

/**
 * Delimited Position is affected definition
*/
class PositionIsAffected
{
private:
    int i;
    bool affected;
    PositionIsAffected();
public:
    PositionIsAffected(int i, bool affected);
    bool isAffected() const;
    void setAffected(bool affected);
    int getI() const;
    void setI(int i);
    bool operator<(const PositionIsAffected &pos_affected) const;
    bool operator>=(const PositionIsAffected &pos_affected) const;
    bool operator==(const PositionIsAffected &pos_affected) const;
    ~PositionIsAffected();
};


/**
 * Class inside utilities
*/
class Random
{
private:
    int l_limit;
    int r_limit;
    Random();
public:
    Random(int l_limit, int r_limit);
    template<typename Iter, typename RandomGenerator> static Iter select_randomly(Iter start, Iter end, RandomGenerator& g);
    template<typename Iter> static Iter select_randomly(Iter start, Iter end);
    int nextInt();
    ~Random();
};