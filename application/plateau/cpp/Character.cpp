#include "../hpp/Header.hpp"

Character::Character(){}

Character::Character(Plateau *plateau, char form, Position *position)
{
    this->form = form;
    this->position = position;
    this->plateau = plateau;
}

// Getters
char Character::getForm()
{
    return this->form;
}

Position* Character::getPosition()
{
    return this->position;
}

// Setters
void Character::setForm(char form)
{
    this->form = form;
}

void Character::setPosition(Position *position)
{
    this->position = position;
}

bool Character::move()
{
    return false;
}

bool Character::moveUp()
{
    return false;
}

bool Character::moveDown()
{
    return false;
}

bool Character::moveLeft()
{
    return false;
}

bool Character::moveRight()
{
    return false;
}

// Swap two Characters
void Character::swap(Character *character)
{
    std::map<int, Character*> inside_affectations = this->plateau->get_inside_affectations();
    int index_t = this->plateau->getIndex(this->getPosition());
    int index_c = this->plateau->getIndex(character->getPosition());
    Character *character_t = inside_affectations[index_t];
    inside_affectations[index_t] = character;
    inside_affectations[index_c] = character_t;
}

// Display position
void Character::printPosition()
{
    std::cout << "(" << this->getPosition()->getI() << ", " << this->getPosition()->getJ() << ")";
}

Character::~Character()
{
    
}