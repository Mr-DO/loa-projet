#include "../hpp/Header.hpp"

int main(int argc, char* argv[]){
    CharacterStationary *char_sta = new CharacterStationary(nullptr, 'k', nullptr);
    std::cout << "move up : " << char_sta->moveUp() << std::endl;
    std::cout << "move down : " << char_sta->moveDown() << std::endl;
    std::cout << "move right : " << char_sta->moveRight() << std::endl;
    std::cout << "move left : " << char_sta->moveLeft() << std::endl; 
}