#include "../hpp/Header.hpp"

CharacterMonster::CharacterMonster(Plateau *plateau, char form, Position *position) : Character(plateau, form, position) {}

void CharacterMonster::attitude(Character *character)
{
    char form = character->getForm();
    switch (form)
    {
    case Form::INTRUS :
        character->attitude(this);
        break;
    default:
        break;
    }
}

bool CharacterMonster::move()
{
    Random rand = Random(1, 4); 
    int choise = rand.nextInt();
    switch (choise)
    {
    case 1 :
        return this->moveUp();
    case 2 :
        return this->moveDown();
    case 3 :
        return this->moveLeft();
    default :
        return this->moveRight();
    }
}

bool CharacterMonster::move_(int i, int j)
{
    int index = this->plateau->getIndex(i, j);
    Character *victim = this->plateau->get_inside_affectations().find(index)->second;
    if( victim->getForm() == Form::DIAMS ||
        victim->getForm() == Form::GEURCHARS ||
        victim->getForm() == Form::STREUMONS ||
        victim->getForm() == Form::REUMUS ||
        victim->getForm() == Form::TEUPORS_OPEN ||
        victim->getForm() == Form::TEUPORS_CLOSE)
    {
        return false;
    }
    else if(victim->getForm() == Form::EMPTY)
    {
        this->plateau->character_has_moving(this->plateau->getIndex(victim->getPosition()));
        this->plateau->swap(this->plateau->getIndex(this->getPosition()), this->plateau->getIndex(victim->getPosition()));
        return true; 
    }
    else if (victim->getForm() == Form::INTRUS)
    {
        victim->setForm(Form::EMPTY);
        this->setForm(Form::DEAD);
        this->plateau->character_has_moving(this->plateau->getIndex(victim->getPosition()));
        this->plateau->swap(this->plateau->getIndex(this->getPosition()), this->plateau->getIndex(victim->getPosition()));
        this->plateau->display();
        exit(0);
        return true;
    }
    return false;
}

bool CharacterMonster::moveUp()
{
    Position *pos = this->getPosition();
    if(pos->getI() - 1 <= 0)
    {
        return false;
    }
    return this->move_(pos->getI() - 1, pos->getJ());
}

bool CharacterMonster::moveDown()
{
    Position *pos = this->getPosition();
    if(pos->getI() + 1 >= this->plateau->getHeight() - 1)
    {
        return false;
    }
    return this->move_(pos->getI() + 1, pos->getJ());
}

bool CharacterMonster::moveLeft()
{
    Position *pos = this->getPosition();
    if(pos->getJ() - 1 <= 0)
    {
        return false;
    }
    return this->move_(pos->getI(), pos->getJ() - 1);
}

bool CharacterMonster::moveRight()
{
    Position *pos = this->getPosition();
    if(pos->getJ() + 1 >= this->plateau->getWidth() - 1)
    {
        return false;
    }
    return this->move_(pos->getI(), pos->getJ() + 1);
}

CharacterMonster::~CharacterMonster()
{
    
}