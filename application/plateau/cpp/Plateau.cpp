#include "../hpp/Header.hpp"

Plateau::Plateau():Plateau(8, 8){}

Plateau::Plateau(int dim):Plateau(dim, dim){}

Plateau::Plateau(int width, int height)
{
    this->preInit(width, height);
}

Plateau::Plateau(std::string file_board)
{
    std::map<std::string, std::string> plateau_elements = this->parse_file_board(file_board);
    if(plateau_elements.size() != 8)
    {
        std::cout << "Error parse file .board, ... modify file .board and try agin." << std::endl;
        exit(1);
    }
    int width = this->peekElementOfPlateau(plateau_elements, "width");
    int height = this->peekElementOfPlateau(plateau_elements, "height");
    this->preInit(width, height);
    this->initCharactersNumber();
    int reumusInside = this->peekElementOfPlateau(plateau_elements, "reumusInside");
    int teupors = this->peekElementOfPlateau(plateau_elements, "teupors");
    int streumons = this->peekElementOfPlateau(plateau_elements, "streumons");
    int diams = this->peekElementOfPlateau(plateau_elements, "diams");
    int geurchars = this->peekElementOfPlateau(plateau_elements, "geurchars");
    int nbrTeleports = this->peekElementOfPlateau(plateau_elements, "nbrTeleports");
    if(reumusInside > 0)
    {
        this->setReumusInsideSize(reumusInside);
    }
    if(teupors > 0 && teupors < this->teupors)
    {
        this->setTeuporsSize(teupors);
    }
    if(streumons > 0 && streumons < this->streumons)
    {
        this->setStreumonsSize(streumons);
    }
    if(diams > 0 && diams < this->diams)
    {
        this->setDiamsSize(diams);
    }
    if(geurchars > 0 && geurchars < this->geurchars)
    {
        this->setGeurcharsSize(geurchars);
    }
    if(nbrTeleports >0 && nbrTeleports <= 3)
    {
        this->nbrTeleports = nbrTeleports;
    }
    else
    {
        Random rand = Random(0, 3);
        this->nbrTeleports = rand.nextInt();
    }
}


// Pre init the members variables before setting
void Plateau::preInit(int width, int height)
{
    if(width < 8 || height < 8)
    {
        std::cout << "width or height of plateau can't less than 8!" << std::endl;
        exit(1);
    }
    this->width = width;
    this->height = height;
    this->nbrElements = width * height;
    this->sideElements = 2 * (width + height);
    this->insideElements = this->nbrElements - this->sideElements;
    this->intrus = 1;
}

// Parsing file .board
std::map<std::string, std::string> Plateau::parse_file_board(std::string file_board)
{
    std::map<std::string, std::string> plateau_elements;
    std::ifstream read_file(file_board);
    std::string str_line;
    std::string delimiter = ":";
    if(read_file.is_open())
    {
        while (std::getline(read_file, str_line))
        {
            if(str_line.compare("") != 0)
            {
                size_t pos = 0;
                std::string token_key;
                std::string token_value;
                if((pos = str_line.find(delimiter)) != std::string::npos)
                {
                    token_key = str_line.substr(0, pos);
                    str_line.erase(0, pos + delimiter.length());
                    std::stringstream trimmer;
                    trimmer << token_key;
                    token_key.clear();
                    trimmer >> token_key;
                }
                token_value = str_line;
                plateau_elements.insert(std::pair<std::string, std::string>(token_key, token_value));
            }
        }
        read_file.close();
    }
    return plateau_elements;
}

/**
 * Initialize all parameters.
*/
void Plateau::initCharactersNumber()
{
    this->initTeuporsSize();
    this->initReumusSize();
    this->initReumusInsideSize();
    this->initStreumonsSize();
    this->initDiamsSize();
    this->initGeurcharsSize();
}

void Plateau::initTeuporsSize()
{
    this->teupors = (this->nbrElements * 4) / 64;
}

void Plateau::initReumusSize()
{
    this->reumus = (2 * (this->width * this->height)) - 4 - this->teupors;
}

void Plateau::initReumusInsideSize()
{
    this->reumusInside = (this->nbrElements * 3) / 64;
}

void Plateau::initStreumonsSize()
{
    this->streumons = (this->nbrElements * 4) / 64;
}

void Plateau::initDiamsSize()
{
    this->diams = (this->nbrElements * 2) / 64;
}

void Plateau::initGeurcharsSize()
{
    this->geurchars = (this->nbrElements * 2) / 64;
}

// set variavle with collect all position and set default affected false
void Plateau::initializeCollectionPositions()
{
    for (int j = 0; j < this->width; j++)
    {
        this->side_affectations.insert(std::make_pair<int, Character*>(this->getIndex(0, j), new CharacterStationary(this, Form::REUMUS, new Position(0, j))));
    }
    for (int i = 1; i < this->height - 1; i++)
    {
        this->side_affectations.insert(std::make_pair<int, Character*>(this->getIndex(i, 0), new CharacterStationary(this, Form::REUMUS, new Position(i, 0))));
        for (int j = 1; j < this->width - 1; j++)
        {
            this->inside_affectations.insert(std::make_pair<int, Character*>(this->getIndex(i, j),  new CharacterStationary(this, Form::EMPTY, new Position(i, j))));
        }
        this->side_affectations.insert(std::make_pair<int, Character*>(this->getIndex(i, this->width - 1), new CharacterStationary(this, Form::REUMUS, new Position(i, this->width - 1))));
    }
    for (int j = 0; j < this->width; j++)
    {
        this->side_affectations.insert(std::make_pair<int, Character*>(this->getIndex(this->height - 1, j), new CharacterStationary(this, Form::REUMUS, new Position(this->height - 1, j))));
    }
}

// add characters on plateau
void Plateau::init()
{
    this->initializeCollectionPositions();
    this->addTeuporsReumusSide();
    this->addReumusInside();
    this->addStreumonsInside();
    this->addDiamsInside();
    this->addGeurcharsInside();
    this->addIntrusInside();
}

// add teupors in side
void Plateau::addTeuporsReumusSide()
{   
    std::map<int, Character*>::iterator it;
    Random random = Random(0, this->side_affectations.size() - 1);
    int victim;
    int index;
    bool angle;
    Character* character;
    for (int i = 0; i < this->teupors; i++)
    {
        do
        {
            victim = random.nextInt();
            it = this->side_affectations.begin();
            std::advance(it, victim);
            character = it->second;
            index = this->getIndex(character->getPosition()->getI(), character->getPosition()->getJ());
            angle = index == 0 || index == (this->width - 1) || index == (this->nbrElements - 1) || index == (this->nbrElements - this->width);
        } while ((character->getForm() != Form::REUMUS) || angle);
        character->setForm(Form::TEUPORS_CLOSE);
    }
}

// add element inside
void Plateau::addElementInside(char discriminant, char elem, int size)
{
    std::map<int, Character*>::iterator it;
    Random random = Random(0, this->inside_affectations.size() - 1);
    int victim;
    Character* character;
    for (int i = 0; i < size; i++)
    {
        do
        {
            victim = random.nextInt();
            it = this->inside_affectations.begin();
            std::advance(it, victim);
            character = it->second;
        } while (character->getForm() != discriminant);
        if(elem == Form::REUMUS || elem == Form::GEURCHARS || elem == Form::DIAMS)
        {
            character->setForm(elem);
        }
        else if(elem == Form::STREUMONS)
        {
            it->second = new CharacterMonster(this, elem, character->getPosition());
            delete character;
        }
    }
}

// add reumus in side
void Plateau::addReumusInside()
{
    this->addElementInside(Form::EMPTY, Form::REUMUS, this->reumusInside);
}

// add streumons in side
void Plateau::addStreumonsInside()
{
    this->addElementInside(Form::EMPTY, Form::STREUMONS, this->streumons);
}

// add diams in side
void Plateau::addDiamsInside()
{
    this->addElementInside(Form::EMPTY, Form::DIAMS, this->diams);
}

// add geurchars in side
void Plateau::addGeurcharsInside()
{
    this->addElementInside(Form::EMPTY, Form::GEURCHARS, this->geurchars);
}

// add intrus in side
void Plateau::addIntrusInside()
{
    std::map<int, Character*>::iterator it;
    Random random = Random(0, this->inside_affectations.size() - 1);
    int victim;
    Character* character;
    do
    {
        victim = random.nextInt();
        it = this->inside_affectations.begin();
        std::advance(it, victim);
        character = it->second;
    } while (character->getForm() != Form::EMPTY);
    it->second = new CharacterPlayer(this, Form::INTRUS, character->getPosition());
    this->player = it->second;
    delete character;
}

// Peek plateau element
int Plateau::peekElementOfPlateau(std::map<std::string, std::string> plateau_elements, std::string key)
{
    int tmp = 0;
    if(plateau_elements.count(key))
    {
        tmp = std::stoi(plateau_elements.find(key)->second);
    }
    else
    {
        std::cout << "Error parse file .board, modify file .board and try agin." << std::endl;
        exit(1);
    }
    return tmp;
}

// Open Random gate side
void Plateau::openAnGate()
{
    std::map<int, Character*>::iterator it;
    it = this->side_affectations.begin();
    for ( ; it != this->side_affectations.end(); it++)
    {
        if(it->second->getForm() == Form::TEUPORS_CLOSE)
        {
            it->second->setForm(Form::TEUPORS_OPEN);
            return;
        }
    }
    
}

// Move Character movable
void Plateau::move()
{
    std::map<int, Character*>::iterator it;
    it = this->inside_affectations.begin();
    for(; it != this->inside_affectations.end() ; it++)
    {
        if (it->second->getForm() == Form::STREUMONS && 
            std::find(this->has_moving.begin(), this->has_moving.end(), it->first) == this->has_moving.end())
        {
            it->second->move();
        }
    }
    this->has_moving.clear();
    this->display();
}


// swap two Characters
void Plateau::swap(int key_1, int key_2)
{
    Character *character_1 = this->inside_affectations[key_1];
    Position *pos_1 = character_1->getPosition();
    Position *pos_2 = this->inside_affectations[key_2]->getPosition();

    this->inside_affectations[key_1] = this->inside_affectations[key_2];
    this->inside_affectations[key_1]->setPosition(pos_1);
    this->inside_affectations[key_2] = character_1;
    this->inside_affectations[key_2]->setPosition(pos_2);
    if(this->inside_affectations[key_1]->getForm() == Form::INTRUS ||
        this->inside_affectations[key_2]->getForm() == Form::INTRUS)
    {
        this->display();
    }
}
   
// marque element has moving
void Plateau::character_has_moving(int index)
{
    this->has_moving.push_back(index);
}

// getters
const std::map<int, Character*> &Plateau::get_side_affectations()
{
    return this->side_affectations;
}

const std::map<int, Character*> &Plateau::get_inside_affectations()
{
    return this->inside_affectations;
}

int Plateau::getWidth()
{
    return this->width;
}

int Plateau::getHeight()
{
    return this->height;
}

int Plateau::getIndex(int i, int j)
{
    return ((i * this->width) + j);
}

int Plateau::getIndex(Position *pos)
{
    return this->getIndex(pos->getI(), pos->getJ());
}

int Plateau::getNbrTeleports()
{
    return this->nbrTeleports;
}

int Plateau::getNbrGates()
{
    return this->nbrGates;
}

int Plateau::getLevel()
{
    return this->level;
}

Character *Plateau::getPlayer()
{
    return this->player;
}

// setters
void Plateau::setNbrTeleports(int teleports)
{
    this->nbrTeleports = teleports;
}

void Plateau::setNbrGates(int gates)
{
    this->nbrGates = gates;
}

void Plateau::setLevel(int level)
{
    this->level = level;
}

void Plateau::setTeuporsSize(int teupors)
{
    this->teupors = teupors;
}

void Plateau::setReumusInsideSize(int reumusInside)
{
    this->reumusInside = reumusInside;
}

void Plateau::setStreumonsSize(int streumons)
{
    this->streumons = streumons;
}

void Plateau::setDiamsSize(int diams)
{
    this->diams = diams;
}

void Plateau::setGeurcharsSize(int geurchars)
{
    this->geurchars = geurchars;
}

// increment functions values
void Plateau::incNbrTeleport()
{
    this->nbrTeleports++;
}

void Plateau::incNbrOpenGate()
{
    this->nbrGates++;
}

void Plateau::incLevel()
{
    this->level++;
}


// decrement functions
void Plateau::decNbrTeleport()
{
    this->nbrTeleports--;
}

void Plateau::decNbrOpenGate()
{
    this->nbrGates--;
}

/**
 * Display Plateau
*/

// Display Head
void Plateau::displayHead()
{
    std::cout << std::endl;
    std::cout << "     *****************************************************************" << std::endl;
    std::cout << "     ********************                        *********************" << std::endl;
    std::cout << "     ********************* Welcome Monster Game **********************" << std::endl;
    std::cout << "     ********************                        *********************" << std::endl;
    std::cout << "     *****************************************************************" << std::endl;
}

// Display footer
void Plateau::displayFooter()
{
    std::cout << "     *****************************************************************" << std::endl;
    std::cout << "     *********************       By Mr. DO      **********************" << std::endl;
    std::cout << "     *****************************************************************" << std::endl;
}

// Display Body
void Plateau::display()
{
    std::system("clear");
    Plateau::displayHead();
    std::cout << "     |          " << std::endl;
    for (int j = 0; j < this->width; j++)
    {
        if(j == 0)
        {
            std::cout << "     |          " << this->side_affectations.find(this->getIndex(0, j))->second->getForm() << " " ;
            continue;
        }
        else if(j == this->width - 1)
        {
            std::cout << this->side_affectations.find(this->getIndex(0, j))->second->getForm() ;
            continue;
        }
        std::cout << this->side_affectations.find(this->getIndex(0, j))->second->getForm() << " " ;
    }
    std::cout << "  Téléports: " << this->getNbrTeleports() << " " << std::endl;
    for (int i = 1; i < this->height - 1; i++)
    {
        std::cout << "     |          " << this->side_affectations.find(this->getIndex(i, 0))->second->getForm() << " ";
        for (int j = 1; j < this->width - 1; j++)
        {
            std::cout << this->inside_affectations.find(this->getIndex(i, j))->second->getForm() << " ";
        }
        std::cout << this->side_affectations.find(this->getIndex(i, this->width - 1))->second->getForm();
        if(i == 1) std::cout << "  Diams: " << this->getNbrGates() << std::endl;
        else if(i == 2) std::cout << "  Level: " << this->getLevel() << std::endl;
        else std::cout << std::endl;
        
    }
    for (int j = 0; j < this->width; j++)
    {
        if(j == 0)
        {
            std::cout << "     |          " << this->side_affectations.find(this->getIndex(this->height - 1, j))->second->getForm() << " " ;
            continue;
        }
        else if(j == this->width - 1)
       {
            std::cout << this->side_affectations.find(this->getIndex(this->height - 1, j))->second->getForm() ;
            continue;
        }
        std::cout << this->side_affectations.find(this->getIndex(this->height - 1, j))->second->getForm() << " " ;
    }
    std::cout << std::endl << "     |          " << std::endl;
    Plateau::displayFooter();
}

Plateau::~Plateau()
{
}