#include "../hpp/Header.hpp"

Position::Position():Position(0, 0){}

Position::Position(int i, int j)
{
    this->i = i;
    this->j = j;
}

int Position::getI() const
{
    return this->i;
}

int Position::getJ() const
{
    return this->j;
}

int Position::getIndex(int width) const
{
    return (this->i * width) + this->j;
}

void Position::setI(int i)
{
    this->i = i;
}

void Position::setJ(int j)
{
    this->j = j;
}

Position::~Position()
{
}

bool Position::operator<(const Position &pos) const
{
    if (this->getI() == pos.getI())
    {
        return this->getJ() < pos.getJ();
    }
    return this->getI() < pos.getI();
}

bool Position::operator>=(const Position &pos) const
{
    return !(*this < pos);
}

bool Position::operator==(const Position &pos) const
{
    return (this->getJ() == pos.getJ()) && (this->getI() == pos.getI());
}