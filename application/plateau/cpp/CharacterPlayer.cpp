#include "../hpp/Header.hpp"

CharacterPlayer::CharacterPlayer(Plateau *plateau, char form, Position *position) : Character(plateau, form, position) {}

void CharacterPlayer::attitude(Character *character)
{
    char form = character->getForm();
    switch (form)
    {
    case Form::INTRUS :
        character->attitude(this);
        break;
    default:
        break;
    }
}

bool CharacterPlayer::move()
{
    Random rand = Random(1, 4); 
    int choise = rand.nextInt();
    switch (choise)
    {
    case 1 :
        return this->moveUp();
    case 2 :
        return this->moveDown();
    case 3 :
        return this->moveLeft();
    default :
        return this->moveRight();
    }
}

bool CharacterPlayer::move_(int i, int j)
{
    int index = this->plateau->getIndex(i, j);
    Character *victim;

    if(this->plateau->get_inside_affectations().count(index))
    {
        victim = this->plateau->get_inside_affectations().find(index)->second;
    }
    else if (this->plateau->get_side_affectations().count(index))
    {
        victim = this->plateau->get_side_affectations().find(index)->second;
    }
    else
    {
        return false;
    }
    
    if( victim->getForm() == Form::DIAMS)
    {
        victim->setForm(Form::EMPTY);
        this->plateau->incNbrOpenGate();
        this->plateau->character_has_moving(this->plateau->getIndex(victim->getPosition()));
        this->plateau->openAnGate();
        this->plateau->swap(this->plateau->getIndex(this->getPosition()), this->plateau->getIndex(victim->getPosition()));
        return true;
    }

    else if (victim->getForm() == Form::GEURCHARS)
    {
        victim->setForm(Form::EMPTY);
        this->plateau->incNbrTeleport();
        this->plateau->character_has_moving(this->plateau->getIndex(victim->getPosition()));
        this->plateau->swap(this->plateau->getIndex(this->getPosition()), this->plateau->getIndex(victim->getPosition()));
        return true;
    }

    else if (victim->getForm() == Form::STREUMONS)
    {
        victim->setForm(Form::EMPTY);
        this->setForm(Form::DEAD);
        this->plateau->swap(this->plateau->getIndex(this->getPosition()), this->plateau->getIndex(victim->getPosition()));
        this->plateau->display();
        exit(0);
        return true;
    }

    else if (victim->getForm() == Form::TEUPORS_OPEN)
    {
        pid_t executing_process_id = getpid();
        kill(executing_process_id, SIGUSR1);
    }

    else if (victim->getForm() == Form::TEUPORS_CLOSE)
    {
        return false;
    }

    else if (victim->getForm() == Form::REUMUS)
    {
        return false;
    }
    
    else if(victim->getForm() == Form::EMPTY)
    {
        this->plateau->character_has_moving(this->plateau->getIndex(victim->getPosition()));
        this->plateau->swap(this->plateau->getIndex(this->getPosition()), this->plateau->getIndex(victim->getPosition()));
        return true; 
    }
    return false;
}

bool CharacterPlayer::moveUp()
{
    Position *pos = this->getPosition();
    if(pos->getI() - 1 <= 0)
    {
        Character* character = this->plateau->get_side_affectations().find(this->plateau->getIndex(pos->getI() - 1, pos->getJ()))->second;
        if(character->getForm() != Form::TEUPORS_OPEN)
        {
            return false;
        }
    }
    return this->move_(pos->getI() - 1, pos->getJ());
}

bool CharacterPlayer::moveDown()
{
    Position *pos = this->getPosition();
    if(pos->getI() + 1 >= this->plateau->getHeight() - 1)
    {
        Character* character = this->plateau->get_side_affectations().find(this->plateau->getIndex(pos->getI() + 1, pos->getJ()))->second;
        if(character->getForm() != Form::TEUPORS_OPEN)
        {
            return false;
        }
    }
    return this->move_(pos->getI() + 1, pos->getJ());
}

bool CharacterPlayer::moveLeft()
{
    Position *pos = this->getPosition();
    if(pos->getJ() - 1 <= 0)
    {
        Character* character = this->plateau->get_side_affectations().find(this->plateau->getIndex(pos->getI(), pos->getJ() - 1))->second;
        if(character->getForm() != Form::TEUPORS_OPEN)
        {
            return false;
        }
    }
    return this->move_(pos->getI(), pos->getJ() - 1);
}

bool CharacterPlayer::moveRight()
{
    Position *pos = this->getPosition();
    if(pos->getJ() + 1 >= this->plateau->getWidth() - 1)
    {
        Character* character = this->plateau->get_side_affectations().find(this->plateau->getIndex(pos->getI(), pos->getJ() + 1))->second;
        if(character->getForm() != Form::TEUPORS_OPEN)
        {
            return false;
        }
    }
    return this->move_(pos->getI(), pos->getJ() + 1);
}

CharacterPlayer::~CharacterPlayer()
{
    
}