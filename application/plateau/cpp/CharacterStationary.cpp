#include "../hpp/Header.hpp"

CharacterStationary::CharacterStationary(Plateau *plateau, char form, Position *position) : Character(plateau, form, position) {}

void CharacterStationary::attitude(Character *character)
{
    char form = character->getForm();
    switch (form)
    {
    case Form::INTRUS :
        if(this->getForm() == Form::GEURCHARS || 
            this->getForm() == Form::DIAMS)
        {
            this->setForm(Form::EMPTY);
        }
        break;
    default:
        break;
    }
}

CharacterStationary::~CharacterStationary()
{
    
}
