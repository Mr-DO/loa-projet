#include "../../plateau/hpp/Header.hpp"


class Game
{
private:
    bool status;
    bool is_test;
    std::string file_game;
    std::vector<std::string> levels;
    int level = 1;
    Plateau *plateau;
    Character *player;
    void teleportGame(bool use_cmd_teleportation, std::string file_game);
    void initGame(std::vector<std::string> levels);
    void initGameTest(std::string level);
    void nextLevel();
    std::vector<std::string> parseGameFile(std::string file_game);
public:
    Game(std::string file_game, bool is_test);
    void pauseGame();
    void endGame();
    void teleportGame(bool use_cmd_teleportation);
    void teleportTestGame(bool use_cmd_teleportation);
    bool isTest();
    void movePlayer(char direction);
    void display();
    void moveInsideElements();
    ~Game();
};

class MovePlayer
{
private:
    MovePlayer();
public:
    static const char UP    = 'i';
    static const char DOWN  = 'k';
    static const char LEFT  = 'j';
    static const char RIGHT = 'l';
    static const char TELEP = 't';
    static const char EXIT  = 'e';
};