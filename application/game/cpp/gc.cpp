#include "../hpp/Header.hpp"

std::string get_file_extension(std::string file_name);
std::vector<std::string> parseGameFile(std::string file_game);
bool check_file_exist(const std::string &file_name);
void save_elements_file(std::vector<std::string> input, std::string, std::string output_file);

int main(int argc, char* argv[])
{
    if (argc == 2)
    {
        std::string ext = get_file_extension(argv[1]);
        if(ext.compare("board") != 0)
        {
            std::cout << "     There are some Errors, commande gp need file .board!";
            exit(1);
        }
        if(check_file_exist("plateaux/" + std::string(argv[1])))
        {
            std::cout << "     File : " << argv[1] << " exist, change name of the file and try again!" << std::endl;
            exit(1);
        }
        std::vector<std::string> saving_skeleton_elements;
        std::vector<std::string> skeleton_elements = parseGameFile("plateaux/skeleton.board");
        std::vector<std::string>::iterator it = skeleton_elements.begin();
        for(; it != skeleton_elements.end(); it++)
        {
            char str_number[5];
            int number;
            std::cout << "Enter " << *it << " : " << std::endl;
            do 
            {
                std::cin.getline(str_number, 5);
                number = atoi(str_number);
            } while (number == 0);
            saving_skeleton_elements.push_back(*it + " : " + str_number);
        }
        if(saving_skeleton_elements.size() == skeleton_elements.size())
        {
            save_elements_file(saving_skeleton_elements, "", "plateaux/" + std::string(argv[1]));
        }
        else
        {
            std::cout << "     There are some Errors, in get input to create file .board!";
            exit(1);
        }
        
    }
    else if(argc > 2)
    {
        std::vector<std::string> elements;
        if(get_file_extension(argv[1]).compare("game") == 0)
        {
            if(check_file_exist("games/" + std::string(argv[1])))
            {
                std::cout << "     File : " << argv[1] << " exist, change name of the file and try again!" << std::endl;
                exit(1);
            }
            for(int i = 2; i < argc; i++)
            {
                if(get_file_extension(argv[i]).compare("board") == 0)
                {
                    elements.push_back(argv[i]);
                }
                else
                {
                    std::cout << "     There are some Errors, commande gc need fichier.board or fichier.game fichier.board [fichier.board...]!";
                    exit(1);
                }
            }
            save_elements_file(elements, "./plateaux/", "games/" + std::string(argv[1]));
        }
        else
        {
            std::cout << "     There are some Errors, commande gc need fichier.game!";
            exit(1);
        }
        
    }
    else
    {
        std::cout << "     There are some Errors, commande gp need file (.game o .board).";
        exit(1);
    }
    return 0;
}

std::string get_file_extension(std::string file_name)
{
    std::vector<std::string> elements;
    std::string delimiter = ".";
    size_t pos = 0;
    while ((pos = file_name.find(delimiter)) != std::string::npos)
    {
        std::string token = file_name.substr(0, pos);
        file_name.erase(0, pos + delimiter.length());
        elements.push_back(token);
    }
    elements.push_back(file_name);
    if(elements.size() > 1)
    {
        return elements[elements.size() - 1];
    }
    return "";
}

std::vector<std::string> parseGameFile(std::string file_game)
{
    std::vector<std::string> element_levels;
    std::ifstream read_file_game(file_game);
    std::string str_line;
    if(read_file_game.is_open())
    {
        while (std::getline(read_file_game, str_line))
        {
            std::stringstream trimmer;
            trimmer << str_line;
            str_line.clear();
            trimmer >> str_line;
            element_levels.push_back(str_line);
        }
        read_file_game.close();
    }
    return element_levels;
}

void save_elements_file(std::vector<std::string> input, std::string path, std::string output_file)
{
    std::ofstream file(output_file, std::ios_base::app | std::ios_base::out);
    std::vector<std::string>::iterator it = input.begin();
    for(; it != input.end(); it++)
    {
        file << path + *it + "\n";
    }
}

bool check_file_exist(const std::string &file_name)
{
    if (FILE *file = fopen(file_name.c_str(), "r"))
    {
        fclose(file);
        return true;
    } 
    else
    {
        return false;
    }
}