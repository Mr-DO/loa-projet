#include "../hpp/Header.hpp"

std::string get_file_extension(std::string file_name);
bool check_file_exist(const std::string &file_name);
void sigalrm_handler(int sig);
void sigsigusr1_handler(int sig);

static Game *game;

int main(int argc, char* argv[])
{
    signal(SIGALRM, &sigalrm_handler);
    signal(SIGUSR1, &sigsigusr1_handler);
    alarm(2);
    if(argc != 2)
    {
        std::cout << "     There are some Errors, commande gp need file (.game o .board).";
        exit(1);
    }
    else
    {
        std::string ext = get_file_extension(argv[1]);
        if(ext.compare("game") == 0)
        {
            if(!check_file_exist("games/" + std::string(argv[1])))
            {
                std::cout << "     Errors, File .game don't exist!" << std::endl;
                exit(1);
            }
            game = new Game("games/" + std::string(argv[1]), false);
            game->display();
            while (true)
            {
                int c;
                std::cout << "Enter : i => Up, j => Left, l => Right, k => Down and e for exit()" << std::endl;
                do 
                {
                    c=getchar();
                    game->movePlayer(c);
                    while ((c = getchar()) != '\n' && c != EOF) { }
                } while (true);
            }
        }
        else if(ext.compare("board") == 0)
        {
            if(!check_file_exist("plateaux/" + std::string(argv[1])))
            {
                std::cout << "     Errors, File .board don't exist!" << std::endl;
                exit(1);
            }
            game = new Game("plateaux/" + std::string(argv[1]), true);
            game->display();
            while (true)
            {
                int c;
                std::cout << "Enter : i => Up, j => Left, l => Right, k => Down and e for exit()" << std::endl;
                do 
                {
                    c=getchar();
                    game->movePlayer(c);
                    while ((c = getchar()) != '\n' && c != EOF) { }
                } while (true);
            }
        }
        else
        {
            std::cout << "Parsing File .board or .game has fail. change file and try again!" << std::endl;
            exit(1);
        }
        
    }
    return 0;
}

std::string get_file_extension(std::string file_name)
{
    std::vector<std::string> elements;
    std::string delimiter = ".";
    size_t pos = 0;
    while ((pos = file_name.find(delimiter)) != std::string::npos)
    {
        std::string token = file_name.substr(0, pos);
        file_name.erase(0, pos + delimiter.length());
        elements.push_back(token);
    }
    elements.push_back(file_name);
    if(elements.size() > 1)
    {
        return elements[elements.size() - 1];
    }
    return "";
}

void sigalrm_handler(int sig)
{
    Random rand = Random(0, 20);
    game->moveInsideElements();
    alarm(2);
}

void sigsigusr1_handler(int sig)
{
    if (sig == SIGUSR1)
    {
        if(game->isTest())
        {
            game->teleportTestGame(false);
        }
        else
        {
            game->teleportGame(true);
        }
        
    }
    else if (sig == SIGUSR2)
    {
        std::cout << "Game Test is Done! You can do another Test or play at Game." << std::endl;
        game->movePlayer('e');
    }
    
}

bool check_file_exist(const std::string &file_name)
{
    if (FILE *file = fopen(file_name.c_str(), "r"))
    {
        fclose(file);
        return true;
    } 
    else
    {
        return false;
    }
}