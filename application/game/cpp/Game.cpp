#include "../hpp/Header.hpp"

Game::Game(std::string file_game, bool is_test) {
    this->is_test = is_test;
    this->file_game = file_game;
    if(!is_test)
    {
        this->initGame(this->parseGameFile(file_game));
    }
    else
    {
        this->initGameTest(file_game);
    }
    
}

void Game::initGame(std::vector<std::string> levels)
{
    std::vector<std::string>::iterator it_level = levels.begin();
    this->levels = levels;
    it_level = levels.begin();
    if(it_level != levels.end())
    {
        std::cout << *it_level << std::endl;
        this->initGameTest(*it_level);
    }
}

void Game::nextLevel()
{
    this->level++;
}

void Game::initGameTest(std::string level)
{
    this->plateau = new Plateau(level);
    this->plateau->init();
    this->player = this->plateau->getPlayer();
}

std::vector<std::string> Game::parseGameFile(std::string file_game)
{
    std::vector<std::string> element_levels;
    std::ifstream read_file_game(file_game);
    std::string str_line;
    if(read_file_game.is_open())
    {
        while (std::getline(read_file_game, str_line))
        {
            std::stringstream trimmer;
            trimmer << str_line;
            str_line.clear();
            trimmer >> str_line;
            element_levels.push_back(str_line);
        }
        read_file_game.close();
    }
    return element_levels;
}

void Game::display()
{
    this->plateau->display();
}

void Game::moveInsideElements()
{
    this->plateau->move();
}

void Game::endGame()
{
    
}

bool Game::isTest()
{
    return this->is_test;
}

void Game::movePlayer(char direction)
{
    switch (direction)
    {
        case MovePlayer::UP:
            this->player->moveUp();
            break;
        case MovePlayer::LEFT:
            this->player->moveLeft();
            break;
        case MovePlayer::RIGHT:
            this->player->moveRight();
            break;
        case MovePlayer::DOWN:
            this->player->moveDown();
            break;
        case MovePlayer::TELEP:
            if(this->plateau->getNbrTeleports() > 0)
            {
                if(this->isTest())
                {
                    this->teleportTestGame(true);
                }
                else
                {
                    this->teleportGame(true);
                }
                
                this->plateau->display();
            }
            break;
        case MovePlayer::EXIT:
            std::cout << "End Game, you can play again." << std::endl;
            exit(0);
            break;
        default :
            break;
    }
}

void Game::teleportGame(bool use_cmd_teleportation, std::string file_game)
{
    int nbTele = this->plateau->getNbrTeleports();
    int nbGate = this->plateau->getNbrGates();
    int level = this->plateau->getLevel();

    Character *old_player = this->player;
    Plateau *old_plateau = this->plateau;

    this->plateau = new Plateau(file_game);
    this->plateau->init();
    this->player = this->plateau->getPlayer();
    this->plateau->setNbrTeleports(use_cmd_teleportation ? nbTele - 1 : nbTele);
    this->plateau->setNbrGates(nbGate);
    this->plateau->setLevel(this->isTest() ? level : level + 1);
    
    this->plateau->display();

    delete old_player;
    delete old_plateau;
}

void Game::teleportGame(bool use_cmd_teleportation)
{   
    std::vector<std::string>::iterator it_level = levels.begin();
    std::advance(it_level, this->level);
    if(it_level != this->levels.end())
    {
        this->teleportGame(use_cmd_teleportation, * it_level);
        this->nextLevel();
        return ;
    }
    std::cout << "You are complete all Levels, you can create ohter game and play!" << std::endl;
    exit(0);
}

void Game::teleportTestGame(bool use_cmd_teleportation)
{
    this->teleportGame(use_cmd_teleportation, this->file_game);
}

Game::~Game()
{
}
