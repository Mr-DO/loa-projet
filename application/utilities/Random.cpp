#include "../plateau/hpp/Header.hpp"


Random::Random(){}

Random::Random(int l_limit, int r_limit)
{
    this->l_limit = l_limit;
    this->r_limit = r_limit;
}

int Random::nextInt()
{
    srand(time(nullptr));
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(l_limit,r_limit);
    return dist6(rng);
}

template<typename Iter, typename RandomGenerator> Iter Random::select_randomly(Iter start, Iter end, RandomGenerator& g)
{
    std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
    std::advance(start, dis(g));
    return start;
}

template<typename Iter> Iter Random::select_randomly(Iter start, Iter end)
{
    static std::random_device rd;
    static std::mt19937 gen(rd());
    return select_randomly(start, end, gen);
}

Random::~Random()
{
}
