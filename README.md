# LOA-projet

Projet de Langage à Objets Avancé année scolaire 2019-2020 (marster 1).
La réalisation d'un jeu avec interface grafique textuelle. 

|************************************************************************|
|------------ Licence DIAKITE Ousmane, tout droits reservés -------------|
**************************************************************************


Instruction sur le fonctionnement du Jeux.

* Dans le repertoire ./apllication se trouve toute la Logique du Jeux.

* Dans le repertoire ./uml se trouve le fichiers relative aux diagrammes UML et les differents contrats.

* Dans le ./application/game se trouvent les deux commandes pricipales ./gc(pour la creation d'un jeux ou un plateau) et ./gp(pour la creation d'un jeux ou du test d'un plateau) 
    - Dans le repertoire ./application/plateau se trouve toute la logique pour la creation d'un plateau.
    - Dans le repertoire ./application/game aussi se trouve toute la logique pour la creation d'un jeux.

1) Creation d'un jeux(suite ordonnée de passage de plateau) et d'un test de plateau.

    1-1) pour la creation d'un plateau, aller dans le repertoire ./application/game, ensuite entrer la commande ./gc suivie d'un nom fichier .board qui n'existe pas.
        Et esuite entrer les informations demandées.
        exemple : ./gc fichier.board

    1-2) pour la creation d'un jeux, aller toujours dans le repertoire ./application/game, ensuite antrer la commande ./gc suivie d'un nom de fichier .game qui n'existe pas.
        Et d'une liste de fichiers .board qui constituerons la suite de plateaux.
        exemple : ./gc fichier.game fichier.board [fichier.board ...] 

2) Pour jouer a un jeux ou tester un plateau.

    2-1) Pour tester un plateau, il suiffit d'aller dans le repertoire ./application/game et lancer la commande ./gp suivie du nom du fichier .board qui doit avoir été crée au paravant.
        exemple ./gp fichier.board

    2-2) Pour jouer a un jeu, il suiffit d'aller dans le repertoire ./application/game et lancer la commande ./gp suivie du nom du fichier .game qui doit avoir été crée au paravant.
        exemple ./gp fichier.game

3) Pour se deplacer dans le plateau, le joueur doit utiliser les touches i(pour se deplacer vers le haut), k(pour se deplacer vers le bas), j(pour se deplacer vers la gauche) et
    l(pour se deplacer vers la droite), t(pour se teleporter) et e(pour quitter) puis entrer pour executer l'action. Le joueur doit eviter les monstre qui sont symbolisés pa (S) au risque
    de perdre la partie. Les diams($) permettent au jouers d'ouvrir les portes dans lesquelles il peut s'engoufrer pour changer de niveau. Les geurchars(*) permettent au joueur d'avoir des
    options de teleportations a un niveau successive.

4) pour chacun des business logique ./application/game et ./application/plateau existe un Makefile pour la creation des executables, des (.o) et pour le clean.
    Apres la odification d'un fichier dans ces repertoires ou sous repertoire, on doit executer la commande "make" pour prendre en compte les modifications. dans la racines de ces repertoires.

NB: Le diagramme uml a été realisé avec le programme "umlet", facile a installer et geré. https://www.umlet.com/ 
